package com.gitrekt.quora.models;

public class User {

  private String id;
  /** Hashed Password */
  private String password;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String hashedPassword) {
    this.password = hashedPassword;
  }

  @Override
  public String toString() {
    return "User{" + "id='" + id + '\'' + ", password='" + password + '\'' + '}';
  }
}
