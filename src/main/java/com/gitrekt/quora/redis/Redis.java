package com.gitrekt.quora.redis;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/** Represents the connection to Redis service. */
public class Redis {

  private static Redis instance;

  /** Connection to Redis service. */
  private JedisPool redisPool;

  private Redis() {
    String host = System.getenv("REDIS_HOST");
    int port = Integer.parseInt(System.getenv("REDIS_PORT"));

    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
    jedisPoolConfig.setMaxTotal(Integer.parseInt(System.getenv("REDIS_THREAD_POOL_COUNT")));

    redisPool = new JedisPool(jedisPoolConfig, host, port);
  }

  private static class RedisHelper {
    private static final Redis INSTANCE = new Redis();
  }

  public static Redis getInstance() {
    return RedisHelper.INSTANCE;
  }

  /**
   * Returns the connection to the Redis service.
   *
   * @return The Redis connection (client)
   */
  public JedisPool getRedisPool() {
    return redisPool;
  }
}
