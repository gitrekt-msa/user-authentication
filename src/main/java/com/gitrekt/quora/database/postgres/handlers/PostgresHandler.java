package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.commons.dbutils.QueryRunner;

public abstract class PostgresHandler<T> {

  protected final String tableName;
  protected Class<T> mapper;
  protected QueryRunner runner;

  /**
   * Postgres Handler Constructor.
   *
   * @param table Table name.
   * @param mapper Class to map rows to.
   */
  public PostgresHandler(String table, Class<T> mapper) {
    tableName = table;
    this.mapper = mapper;
    runner = new QueryRunner(PostgresConnection.getInstance().dataSource);
  }

  protected void call(String sql, int[] types, Object... params) throws SQLException {
    // DbUtils does not support calling procedures?
    Connection connection = null;
    try {
      connection = PostgresConnection.getInstance().getConnection();
      CallableStatement callableStatement = connection.prepareCall(sql);
      for (int i = 0; i < types.length; i++) {
        callableStatement.setObject(i + 1, params[i], types[i]);
      }
      callableStatement.execute();
    } finally {
      if (connection != null) {
        connection.close();
      }
    }
  }

  public abstract void insert(Object... params) throws SQLException;

  public abstract void deleteUser(Object... params) throws SQLException;

  public abstract T getUserPassword(Object... params) throws SQLException;

}
