package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.User;

import java.sql.SQLException;
import java.sql.Types;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

public class UsersPostgresHandler extends PostgresHandler<User> {

  public UsersPostgresHandler() {
    super("Users", User.class);
  }

  public void deleteUser(Object... params) throws SQLException {
    super.call("CALL delete_user_moderator(?)", new int[]{Types.OTHER}, params);
  }

  /**
   * Inserts a user into the database by calling the `Insert_User` procedure.
   *
   * @param params The parameters to the procedure
   * @throws SQLException if an error occurred when inserting into the database
   */
  @Override
  public void insert(Object... params) throws SQLException {
    int[] types = new int[params.length];
    types[0] = Types.OTHER;
    for (int i = 1; i < params.length; i++) {
      types[i] = Types.VARCHAR;
    }
    super.call("CALL Insert_User(?, ?, ?, ?, ?, ?)", types, params);
  }

  /**
   * Calls the `Get_User_ID_And_Password` SQL Function to retrieve the user's `id` and hashed
   * `password`.
   *
   * @param param The parameter to the procedure (email)
   * @return The user with the corresponding fields populated from the database
   * @throws SQLException if an error occurred when inserting into the database
   */
  @Override
  public User getUserPassword(Object... param) throws SQLException {
    ResultSetHandler<User> users =
        new BeanHandler<>(mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
    return runner.query("SELECT * FROM Get_User_ID_And_Password(?)",users, param);
  }
}
