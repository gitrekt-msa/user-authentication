package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import java.sql.SQLException;
import java.util.HashMap;

public class DeleteCommand extends Command {

  private static final String[] argumentNames = new String[] {"userId"};

  public DeleteCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException, AuthenticationException {
    checkArguments(argumentNames);
    String userId = (String) args.get("userId");
    postgresHandler.deleteUser(userId);

    return "User Deleted successfully";
  }
}
