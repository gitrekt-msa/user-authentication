package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.authentication.Jwt;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.User;
import com.gitrekt.quora.redis.Redis;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import redis.clients.jedis.Jedis;

/**
 * This command is responsible for login. It does so by getting the user by the provided email from
 * the database, and checking his hashed password against the provided one. It throws an exception
 * if no user with the corresponding email exists or the password is incorrect, otherwise it returns
 * a JWT.
 */
public class LoginCommand extends Command {

  private static final String[] argumentNames = new String[] {"email", "password"};

  private static final int JWT_EXPIRE = Integer.parseInt(System.getenv("JWT_EXPIRE"));

  public LoginCommand() {}

  public LoginCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public String execute() throws SQLException, BadRequestException, AuthenticationException {
    checkArguments(argumentNames);
    String email = (String) args.get("email");
    String password = (String) args.get("password");

    User user = (User) postgresHandler.getUserPassword(email);
    if (user == null) {
      throw new BadRequestException("No account with this email was found");
    }
    if (user.getPassword() != null && BCrypt.checkpw(password, user.getPassword())) {
      Map<String, String> claims = new HashMap<>();
      claims.put("userId", user.getId());
      //      claims.put("scope", "user");
      String token = Jwt.generateToken(claims, JWT_EXPIRE);
      try (Jedis redisClient = Redis.getInstance().getRedisPool().getResource()) {
        redisClient.set(token, user.getId());
        redisClient.expire(token, JWT_EXPIRE * 1000);
      }
      return token;
    }
    throw new AuthenticationException("Incorrect Password");
  }
}
