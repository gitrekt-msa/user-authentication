import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.commands.handlers.LoginCommand;
import com.gitrekt.quora.database.postgres.handlers.PostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.User;
import java.sql.SQLException;
import java.util.HashMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LoginCommandTest {

  private PostgresHandler<User> postgresHandler;
  private User user;

  @Before
  public void initialize() {
    postgresHandler = mock(UsersPostgresHandler.class);
    user = new User();
  }

  @Test
  public void shouldLoginUserAndReturnJwt()
      throws SQLException, BadRequestException, AuthenticationException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "123456");

    user.setId("1");
    user.setPassword("$2a$12$eEnAoe9/slBhTDPVkvfmke3ACcVgzpKf/UDA9l2S8o9uNITQzXgPm");
    when(postgresHandler.getUserPassword(map.get("email"))).thenReturn(user);

    Command loginCommand = spy(new LoginCommand(map));
    loginCommand.setPostgresHandler(postgresHandler);

    Object result = loginCommand.execute();

    verify(loginCommand, times(1).description("Should only check arguments once"))
        .checkArguments(new String[] {"email", "password"});
    verify(postgresHandler, times(1).description("Should only fetch user once"))
        .getUserPassword(map.get("email"));
    Assert.assertNotNull("Jwt should not be null", result);
  }

  @Test(expected = BadRequestException.class)
  public void shouldThrowAnErrorIfNoUserWasFound()
      throws SQLException, BadRequestException, AuthenticationException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "123456");

    when(postgresHandler.getUserPassword(map.get("email"))).thenReturn(null);

    Command loginCommand = new LoginCommand(map);
    loginCommand.setPostgresHandler(postgresHandler);

    loginCommand.execute();
  }

  @Test(expected = AuthenticationException.class)
  public void shouldThrowAnErrorIfPasswordIsNotCorrect()
      throws SQLException, BadRequestException, AuthenticationException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "Incorrect Password");

    user.setId("1");
    user.setPassword("$2a$12$eEnAoe9/slBhTDPVkvfmke3ACcVgzpKf/UDA9l2S8o9uNITQzXgPm");
    when(postgresHandler.getUserPassword(map.get("email"))).thenReturn(user);

    Command loginCommand = new LoginCommand(map);
    loginCommand.setPostgresHandler(postgresHandler);

    loginCommand.execute();
  }
}
