import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.handlers.SignUpCommand;
import com.gitrekt.quora.database.postgres.handlers.PostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.User;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SignUpCommandTest {

  private PostgresHandler<User> postgresHandler;

  @Before
  public void initialize() {
    postgresHandler = mock(UsersPostgresHandler.class);
  }

  @Test
  public void shouldSignUpUserSuccessfully() throws BadRequestException, SQLException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "123456");
    map.put("confirmPassword", "123456");
    map.put("username", "Some Username");
    map.put("firstName", "First Name");
    map.put("lastName", "Last Name");

    SignUpCommand signUpCommand = spy(new SignUpCommand(map));
    signUpCommand.setPostgresHandler(postgresHandler);

    String result = signUpCommand.execute();

    verify(signUpCommand, times(1).description("Should verify arguments once"))
        .validate(
            (String) map.get("email"),
            (String) map.get("password"),
            (String) map.get("confirmPassword"));
    verify(
        postgresHandler,
            times(1).description("Should only insert user once with the correct arguments"))
        .insert(
            any(UUID.class),
            eq(map.get("email")),
            eq(map.get("username")),
            not(eq((String) map.get("password"))),
            eq(map.get("firstName")),
            eq(map.get("lastName")));

    Assert.assertEquals("Should return a success message", "SignUp Successful!", result);
  }

  @Test(expected = BadRequestException.class)
  public void shouldThrowAnErrorIfArgumentIsMissing() throws BadRequestException, SQLException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "123456");
    map.put("confirmPassword", "123456");
    map.put("firstName", "First Name");
    map.put("lastName", "Last Name");

    SignUpCommand signUpCommand = spy(new SignUpCommand(map));
    signUpCommand.setPostgresHandler(postgresHandler);

    signUpCommand.execute();
  }

  @Test(expected = BadRequestException.class)
  public void shouldThrowAnErrorIfPasswordDoesNotMatch() throws BadRequestException, SQLException {
    HashMap<String, Object> map = new HashMap<>();
    map.put("email", "tester@email.com");
    map.put("password", "123456");
    map.put("confirmPassword", "123");
    map.put("username", "Some Username");
    map.put("firstName", "First Name");
    map.put("lastName", "Last Name");

    SignUpCommand signUpCommand = spy(new SignUpCommand(map));
    signUpCommand.setPostgresHandler(postgresHandler);

    signUpCommand.execute();
  }
}
